Source: libdist-metadata-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>,
           Florian Schlichting <fsfs@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libarchive-zip-perl,
                     libcpan-distnameinfo-perl,
                     libfile-spec-native-perl,
                     libpath-class-perl,
                     libtest-fatal-perl,
                     libtest-mockobject-perl,
                     libtry-tiny-perl,
                     perl,
                     perl (>= 5.15.8) | libmodule-metadata-perl
Standards-Version: 3.9.8
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libdist-metadata-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libdist-metadata-perl.git
Homepage: https://metacpan.org/release/Dist-Metadata

Package: libdist-metadata-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libarchive-zip-perl,
         libcpan-distnameinfo-perl,
         libfile-spec-native-perl,
         libpath-class-perl,
         libtry-tiny-perl,
         perl (>= 5.15.8) | libmodule-metadata-perl
Description: module for getting information about a perl module distribution
 Dist::Metadata provides an easy interface for getting various metadata about
 a Perl module distribution.
 .
 This is mostly a wrapper around CPAN::Meta providing an easy interface to
 find and load the meta file from a tar.gz file. A dist can also be
 represented by a directory or merely a structure of data.
 .
 If the dist does not contain a meta file the module will attempt to determine
 some of that data from the dist.
